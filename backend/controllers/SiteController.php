<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\PasswordForm;
use backend\models\User;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            /*'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],*/
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) return $this->redirect(['login']);
            
        return $this->render('index');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionRegister()
    {
        $model = new User();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post())) {
            $model->setPassword($model->password);
            $model->generateAuthKey();

            if ($model->save()) {
                $auth = \Yii::$app->authManager;
                $role = $auth->getRole('user');
                $auth->assign($role, $model->id);

                if (Yii::$app->getUser()->login($model)) {
                    return $this->goHome();
                }
            } else {
                Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
            }
        }

        return $this->render('register', [
            'model' => $model,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /*
     * Change Password
     */
    public function actionChangePassword(){
        $model      = new PasswordForm;
        $modeluser  = User::findOne(Yii::$app->user->id);
     
        if($model->load(Yii::$app->request->post())){
            if($model->validate()){
                try{
                    $password = $_POST['PasswordForm']['newpass'];
                    $modeluser->setPassword($password);
                    $modeluser->generateAuthKey();

                    if($modeluser->save(false)){
                        Yii::$app->getSession()->setFlash(
                            'success','Password changed.'
                        );
                        return $this->redirect(['index']);
                    }else{
                        Yii::$app->getSession()->setFlash(
                            'error','Password not changed.'
                        );
                        return $this->render('changepassword',['model'=>$model]);
                    }
                }catch(Exception $e){
                    Yii::$app->getSession()->setFlash(
                        'error',"{$e->getMessage()}"
                    );
                    return $this->render('changepassword',['model'=>$model]);
                }
            }else{
                return $this->render('changepassword',['model'=>$model]);
            }
        }else{
            return $this->render('changepassword',['model'=>$model]);
        }
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionProfile()
    {
        return $this->render('profile', [
            'model' => User::findOne(Yii::$app->user->id),
        ]);
    }

    public function actionProfileUpdate()
    {
        $model = User::findOne(Yii::$app->user->id);
        // $model->scenario = User::SCENARIO_UPDATE;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('profile-update', [
            'model' => $model,
        ]);
    }
}
