<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Paket */

$this->title = 'Create Paket';
$this->params['breadcrumbs'][] = ['label' => 'Paket', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paket-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
