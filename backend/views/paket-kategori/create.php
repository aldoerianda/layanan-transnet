<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\PaketKategori */

$this->title = 'Create Paket Kategori';
$this->params['breadcrumbs'][] = ['label' => 'Paket Kategori', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paket-kategori-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
