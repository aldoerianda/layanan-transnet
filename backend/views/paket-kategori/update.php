<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PaketKategori */

$this->title = 'Update Paket Kategori: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Paket Kategori', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="paket-kategori-update box-- box-warning--">

    <!-- <div class="box-header"></div> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
