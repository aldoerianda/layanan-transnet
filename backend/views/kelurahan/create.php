<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Kelurahan */

$this->title = 'Create Kelurahan';
$this->params['breadcrumbs'][] = ['label' => 'Kelurahan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelurahan-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
