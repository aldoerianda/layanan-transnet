<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\PaketDetail */

$this->title = 'Create Paket Detail';
$this->params['breadcrumbs'][] = ['label' => 'Paket Detail', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paket-detail-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
