<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PaketDetail */

$this->title = 'Update Paket Detail: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Paket Detail', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="paket-detail-update box-- box-warning--">

    <!-- <div class="box-header"></div> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
