<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Pelanggan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pelanggan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="pelanggan-view box-- box-info--">

    <div class="box-body--">
        <p>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> '. 'Update', ['update', 'id' => $model->id], [
            'class' => 'btn btn-warning',
        ]) ?>
        <?= Html::a('<i class="glyphicon glyphicon-trash"></i> ' . 'Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        </p>

        <div class="detail-view-container">
        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                // 'id',
                'no_pelanggan',
                'email:email',
                'nama',
                'no_handphone',
                'provinsi.nama:text:Provins',
                'kota.nama:text:Kota',
                'kecamatan.nama:text:Kecamatan',
                'kelurahan.nama:text:Kelurahan',
                'lat',
                'lng',
                'alamat:ntext',
                'paket.nama_paket:text:Paket',
                'tgl_pendaftaran',
                'perusahaan',
                'password_hash',
                // 'created_at:datetime',
                // 'updated_at:datetime',
                // 'createdBy.username:text:Created By',
                // 'updatedBy.username:text:Updated By',
                'password_reset_token',
                'auth_key',
                'status:integer',
            ],
        ]) ?>
        </div>
    </div>
</div>
