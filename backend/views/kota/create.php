<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Kota */

$this->title = 'Create Kota';
$this->params['breadcrumbs'][] = ['label' => 'Kota', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kota-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
