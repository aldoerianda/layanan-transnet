<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tagihan".
 *
 * @property int $id
 * @property string $bulan
 * @property string $tahun
 * @property string $total
 * @property string $status
 * @property int $pelanggan_id
 * @property string $paid_by
 * @property string $metoda
 * @property string $tanggal
 * @property int $confirm_by
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property DetailTagihan[] $detailTagihans
 * @property Pelanggan $pelanggan
 * @property Transaksi[] $transaksis
 */
class Tagihan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tagihan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bulan', 'tahun', 'total', 'status', 'pelanggan_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'required'],
            [['pelanggan_id', 'confirm_by', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['tanggal'], 'safe'],
            [['bulan', 'tahun', 'total', 'status'], 'string', 'max' => 50],
            [['paid_by', 'metoda'], 'string', 'max' => 45],
            [['pelanggan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pelanggan::className(), 'targetAttribute' => ['pelanggan_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bulan' => 'Bulan',
            'tahun' => 'Tahun',
            'total' => 'Total',
            'status' => 'Status',
            'pelanggan_id' => 'Pelanggan ID',
            'paid_by' => 'Paid By',
            'metoda' => 'Metoda',
            'tanggal' => 'Tanggal',
            'confirm_by' => 'Confirm By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetailTagihans()
    {
        return $this->hasMany(DetailTagihan::className(), ['tagihan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPelanggan()
    {
        return $this->hasOne(Pelanggan::className(), ['id' => 'pelanggan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaksis()
    {
        return $this->hasMany(Transaksi::className(), ['tagihan_id' => 'id']);
    }
}
