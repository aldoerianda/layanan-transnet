<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "transaksi".
 *
 * @property integer $id
 * @property integer $jenis_transaksi_id
 * @property double $biaya
 * @property string $tanggal
 * @property integer $user_id
 * @property string $keterangan
 * @property integer $tagihan_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property JenisTransaksi $jenisTransaksi
 * @property Tagihan $tagihan
 * @property User $user
 */
class Transaksi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaksi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis_transaksi_id', 'biaya', 'tanggal', 'user_id', 'keterangan', 'tagihan_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'required'],
            [['jenis_transaksi_id', 'user_id', 'tagihan_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['biaya'], 'number'],
            [['tanggal'], 'safe'],
            [['keterangan'], 'string'],
            [['jenis_transaksi_id'], 'exist', 'skipOnError' => true, 'targetClass' => JenisTransaksi::className(), 'targetAttribute' => ['jenis_transaksi_id' => 'id']],
            [['tagihan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tagihan::className(), 'targetAttribute' => ['tagihan_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_transaksi_id' => 'Jenis Transaksi',
            'biaya' => 'Biaya',
            'tanggal' => 'Tanggal',
            'user_id' => 'User',
            'keterangan' => 'Keterangan',
            'tagihan_id' => 'Tagihan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisTransaksi()
    {
        return $this->hasOne(JenisTransaksi::className(), ['id' => 'jenis_transaksi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagihan()
    {
        return $this->hasOne(Tagihan::className(), ['id' => 'tagihan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
