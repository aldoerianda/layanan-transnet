<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "paket_detail".
 *
 * @property integer $id
 * @property integer $paket_id
 * @property string $item
 * @property double $biaya
 *
 * @property Paket $paket
 */
class PaketDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'paket_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paket_id', 'item', 'biaya'], 'required'],
            [['paket_id'], 'integer'],
            [['biaya'], 'number'],
            [['item'], 'string', 'max' => 50],
            [['paket_id'], 'exist', 'skipOnError' => true, 'targetClass' => Paket::className(), 'targetAttribute' => ['paket_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'paket_id' => 'Paket',
            'item' => 'Item',
            'biaya' => 'Biaya',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaket()
    {
        return $this->hasOne(Paket::className(), ['id' => 'paket_id']);
    }
}
