<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "detail_paket".
 *
 * @property integer $id
 * @property integer $paket_id
 * @property string $item
 * @property double $biaya
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class DetailPaket extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'detail_paket';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paket_id', 'item', 'biaya'], 'required'],
            [['paket_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['biaya'], 'number'],
            [['item'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'paket_id' => 'Paket',
            'item' => 'Item',
            'biaya' => 'Biaya',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    public function getPaket()
    {
        return $this->hasOne(Paket::className(), ['id' => 'paket_id']);
    }
}
