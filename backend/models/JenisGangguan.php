<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "jenis_gangguan".
 *
 * @property integer $id
 * @property string $nama_gangguan
 * @property string $keterangan
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Tiket[] $tikets
 */
class JenisGangguan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenis_gangguan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_gangguan', 'keterangan'], 'required'],
            [['keterangan'], 'string'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['nama_gangguan'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_gangguan' => 'Nama Gangguan',
            'keterangan' => 'Keterangan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTikets()
    {
        return $this->hasMany(Tiket::className(), ['jenis_gangguan_id' => 'id']);
    }
}
