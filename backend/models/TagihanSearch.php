<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Tagihan;

/**
 * TagihanSearch represents the model behind the search form about `backend\models\Tagihan`.
 */
class TagihanSearch extends Tagihan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pelanggan_id', 'confirm_by', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['bulan', 'tahun', 'total', 'status', 'paid_by', 'metoda', 'tanggal'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tagihan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'pelanggan_id' => $this->pelanggan_id,
            'tanggal' => $this->tanggal,
            'confirm_by' => $this->confirm_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'bulan', $this->bulan])
            ->andFilterWhere(['like', 'tahun', $this->tahun])
            ->andFilterWhere(['like', 'total', $this->total])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'paid_by', $this->paid_by])
            ->andFilterWhere(['like', 'metoda', $this->metoda]);

        return $dataProvider;
    }
}
