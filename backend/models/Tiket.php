<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tiket".
 *
 * @property integer $id
 * @property integer $jenis_gangguan_id
 * @property integer $pelanggan_id
 * @property string $tanggal
 * @property string $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Pelanggan $pelanggan
 * @property JenisGangguan $jenisGangguan
 */
class Tiket extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tiket';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis_gangguan_id', 'pelanggan_id', 'tanggal', 'status'], 'required'],
            [['jenis_gangguan_id', 'pelanggan_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['tanggal','catatan'], 'safe'],
            [['status'], 'string', 'max' => 50],
            [['pelanggan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pelanggan::className(), 'targetAttribute' => ['pelanggan_id' => 'id']],
            [['jenis_gangguan_id'], 'exist', 'skipOnError' => true, 'targetClass' => JenisGangguan::className(), 'targetAttribute' => ['jenis_gangguan_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jenis_gangguan_id' => 'Jenis Gangguan',
            'pelanggan_id' => 'Pelanggan',
            'tanggal' => 'Tanggal',
            'status' => 'Status',
            'catatan' => 'Catatan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPelanggan()
    {
        return $this->hasOne(Pelanggan::className(), ['id' => 'pelanggan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisGangguan()
    {
        return $this->hasOne(JenisGangguan::className(), ['id' => 'jenis_gangguan_id']);
    }
}
