<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pelanggan".
 *
 * @property integer $id
 * @property string $no_pelanggan
 * @property string $email
 * @property string $nama
 * @property string $no_handphone
 * @property integer $provinsi_id
 * @property integer $kota_id
 * @property integer $kecamatan_id
 * @property integer $kelurahan_id
 * @property string $lat
 * @property string $lng
 * @property string $alamat
 * @property integer $paket_id
 * @property string $tgl_pendaftaran
 * @property string $perusahaan
 * @property string $password_hash
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $password_reset_token
 * @property string $auth_key
 * @property integer $status
 *
 * @property Provinsi $provinsi
 * @property Kelurahan $kelurahan
 * @property Kota $kota
 * @property Kecamatan $kecamatan
 * @property Paket $paket
 * @property Tagihan[] $tagihans
 * @property Tiket[] $tikets
 */
class Pelanggan extends \common\models\Pelanggan
{
    public $password;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pelanggan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_pelanggan', 'email', 'nama', 'no_handphone', 'provinsi_id', 'kota_id', 'kecamatan_id', 'kelurahan_id', 'alamat', 'paket_id', 'tgl_pendaftaran', 'perusahaan', 'password_hash'], 'required'],
            [['provinsi_id', 'kota_id', 'kecamatan_id', 'kelurahan_id', 'paket_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'status'], 'integer'],
            [['alamat','lat', 'lng'], 'string'],
            [['tgl_pendaftaran'], 'safe'],
            [['no_pelanggan'], 'string', 'max' => 12],
            [['email', 'nama', 'perusahaan'], 'string', 'max' => 50],
            [['no_handphone'], 'string', 'max' => 15],
            [['lat', 'lng'], 'string', 'max' => 20],
            [['password_hash', 'password_reset_token', 'auth_key'], 'string', 'max' => 255],
            [['provinsi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Provinsi::className(), 'targetAttribute' => ['provinsi_id' => 'id']],
            [['kelurahan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kelurahan::className(), 'targetAttribute' => ['kelurahan_id' => 'id']],
            [['kota_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kota::className(), 'targetAttribute' => ['kota_id' => 'id']],
            [['kecamatan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kecamatan::className(), 'targetAttribute' => ['kecamatan_id' => 'id']],
            [['paket_id'], 'exist', 'skipOnError' => true, 'targetClass' => Paket::className(), 'targetAttribute' => ['paket_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_pelanggan' => 'No Pelanggan',
            'email' => 'Email',
            'nama' => 'Nama',
            'no_handphone' => 'No Handphone',
            'provinsi_id' => 'Provinsi',
            'kota_id' => 'Kota',
            'kecamatan_id' => 'Kecamatan',
            'kelurahan_id' => 'Kelurahan',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'alamat' => 'Alamat',
            'paket_id' => 'Paket',
            'tgl_pendaftaran' => 'Tgl Pendaftaran',
            'perusahaan' => 'Perusahaan',
            'password_hash' => 'Password Hash',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'password_reset_token' => 'Password Reset Token',
            'auth_key' => 'Auth Key',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvinsi()
    {
        return $this->hasOne(Provinsi::className(), ['id' => 'provinsi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKelurahan()
    {
        return $this->hasOne(Kelurahan::className(), ['id' => 'kelurahan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKota()
    {
        return $this->hasOne(Kota::className(), ['id' => 'kota_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKecamatan()
    {
        return $this->hasOne(Kecamatan::className(), ['id' => 'kecamatan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaket()
    {
        return $this->hasOne(Paket::className(), ['id' => 'paket_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagihans()
    {
        return $this->hasMany(Tagihan::className(), ['pelanggan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTikets()
    {
        return $this->hasMany(Tiket::className(), ['pelanggan_id' => 'id']);
    }
}
