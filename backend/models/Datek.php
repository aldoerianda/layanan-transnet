<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "datek".
 *
 * @property integer $id
 * @property string $ip_olt
 * @property string $odc
 * @property string $odp
 * @property string $port
 * @property string $vlan
 * @property string $redaman
 * @property string $drop_wire
 * @property string $keterangan
 * @property string $sn_ont
 * @property string $ip_management
 * @property string $ip_public
 * @property string $vlan_pelanggan
 */
class Datek extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'datek';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip_olt', 'odc', 'odp', 'port', 'vlan', 'redaman', 'drop_wire', 'keterangan', 'sn_ont', 'ip_management', 'ip_public', 'vlan_pelanggan'], 'required'],
            [['keterangan'], 'string'],
            [['ip_olt', 'ip_management', 'ip_public'], 'string', 'max' => 40],
            [['odc', 'odp', 'port', 'vlan', 'vlan_pelanggan'], 'string', 'max' => 30],
            [['redaman', 'drop_wire', 'sn_ont'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip_olt' => 'Ip Olt',
            'odc' => 'Odc',
            'odp' => 'Odp',
            'port' => 'Port',
            'vlan' => 'Vlan',
            'redaman' => 'Redaman',
            'drop_wire' => 'Drop Wire',
            'keterangan' => 'Keterangan',
            'sn_ont' => 'Sn Ont',
            'ip_management' => 'Ip Management',
            'ip_public' => 'Ip Public',
            'vlan_pelanggan' => 'Vlan Pelanggan',
        ];
    }
}
