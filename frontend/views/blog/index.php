<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\ListView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Informasi');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if (!$dataProvider->models) { ?>
        
        <div class="detail-view-container text-center text-muted" style="padding:50px 20px">
            <p>
            <big>Tidak ada data.</big>
            <br><span class="small">Belum ada informasi tersedia.</span>
            </p>
        </div>

    <?php } else { ?>

        <?php Pjax::begin(); ?>    
        <?= ListView::widget([
            'summary' => false,
            'dataProvider' => $dataProvider,
            'itemOptions' => ['class' => 'item'],
            'itemView' => function ($model, $key, $index, $widget) {
                // return '<h1>'.Html::a(Html::encode($model->name), ['view', 'id' => $model->id]).'</h1>'.StringHelper::byteSubstr($model->content, 0, 500).'...';
                return '
                <div class="box box-info wow fadeInUp">
                <div class="box-header">
                <h3 class="box-title">'.$model->name.'</h3><p class="small">
                    <i>Posted at '.Yii::$app->formatter->asDate($model->updated_at).' by '.$model->updatedBy->username.'</i>
                </p>
                </div>
                <div class="box-body">
                    <div class="wow fadeInDown" data-wow-delay="500ms">'.$model->content.'</div>
                </div>
                ';
            },
        ]) ?>
        <?php Pjax::end(); ?>

    <?php } ?>
</div>
