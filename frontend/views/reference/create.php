<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Form */

$this->title = 'Create Form';
$this->params['breadcrumbs'][] = ['label' => 'Form', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
