<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use backend\models\Form;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FormSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Form';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="form-index box-- box-primary-- box-body--">

    <?php if (Form::find()->where(['is not', 'file', null])->one() === null) {
    echo '
    <div class="detail-view-container text-center text-muted" style="padding:50px 20px">
        <p>
        <big>Tidak ada data.</big>
        <br><span class="small">Belum ada form tersedia untuk didownload.</span>
        </p>
    </div>';
    } else {
        foreach($dataProvider->models as $model) {
            if ($model->file) {
    ?>
    <div class="detail-view-container" style="padding:15px">
        <p>
            <big><?= $model->name ?></big>
            <?php if ($model->description) echo '<br><small class="text-muted">' . $model->description . '</small>'; ?>
        </p>
        <?= Html::a('Download', ['download', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
    </div>
    <?php 
            }
        } 
    } 
    ?>
</div>
