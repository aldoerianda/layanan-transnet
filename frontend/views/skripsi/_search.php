<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SkripsiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="skripsi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'proposal_id') ?>

    <?= $form->field($model, 'examination_date') ?>

    <?= $form->field($model, 'toefl_score') ?>

    <?php // echo $form->field($model, 'file_approval') ?>

    <?php // echo $form->field($model, 'file_toefl') ?>

    <?php // echo $form->field($model, 'file_turnitin_article') ?>

    <?php // echo $form->field($model, 'file_turnitin_skripsi') ?>

    <?php // echo $form->field($model, 'file_publishing') ?>

    <?php // echo $form->field($model, 'file_lecturer_availability') ?>

    <?php // echo $form->field($model, 'file_invitation') ?>

    <?php // echo $form->field($model, 'file_handover') ?>

    <?php // echo $form->field($model, 'file_minutes_of_meeting') ?>

    <?php // echo $form->field($model, 'file_article') ?>

    <?php // echo $form->field($model, 'file_skripsi') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
