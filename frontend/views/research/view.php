<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Research */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Researches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->id;
?>

<div class="research-view boxxx boxxx-info">

    <div class="boxxx-body">
        <p>
        <?= Html::a('<i class="glyphicon glyphicon-print"></i> '. 'Export to PDF', ['pdf', 'id' => $model->id], [
            'class' => 'btn btn-default',
        ]) ?>
        </p>

        <div class="detail-view-container">
        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                // 'id',
                'name',
                'date:date',
                'researcher',
                'description:ntext',
                'created_at:datetime',
                'updated_at:datetime',
                'createdBy.username:text:Created By',
                'updatedBy.username:text:Updated By',
            ],
        ]) ?>
        </div>
    </div>
</div>
