<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Research */

$this->title = 'Update Research: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Researches', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="research-update boxxx boxxx-warning">

    <div class="boxxx-header"></div>

    <div class="boxxx-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
