<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use backend\models\JenisGangguan;
use backend\models\Pelanggan;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TiketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tiket';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="tiket-index box-- box-primary-- box-body--">

    <?php Pjax::begin(); ?>    <?php 
        $exportColumns = [
            [
                'class' => 'yii\grid\SerialColumn',
            ],
            'id',
            'jenisGangguan.nama_gangguan:text:Jenis gangguan',
            'pelanggan.nama:text:Pelanggan',
            'tanggal:date',
            'status',
            'catatan',
            'created_at:datetime',
            'updated_at:datetime',
            'createdBy.username:text:Created By',
            'updatedBy.username:text:Updated By',
        ];

        $exportMenu = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $exportColumns,
            'filename' => 'Tiket',
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Export',
                'class' => 'btn btn-default'
            ],
            'target' => ExportMenu::TARGET_SELF,
            'exportConfig' => [
                ExportMenu::FORMAT_CSV => false,
                ExportMenu::FORMAT_EXCEL => false,
                ExportMenu::FORMAT_HTML => false,
            ],
            'styleOptions' => [
                ExportMenu::FORMAT_EXCEL_X => [
                    'font' => [
                        'color' => ['argb' => '00000000'],
                    ],
                    'fill' => [
                      //  'type' => PHPExcel_Style_Fill::FILL_NONE,
                        'color' => ['argb' => 'DDDDDDDD'],
                    ],
                ],
            ],
            'pjaxContainerId' => 'grid',
        ]);

        $gridColumns = [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['class' => 'text-right serial-column'],
                'contentOptions' => ['class' => 'text-right serial-column'],
            ],
            // [
            //     'contentOptions' => ['class' => 'action-column nowrap text-left'],
            //     'class' => 'yii\grid\ActionColumn',
            //     'buttons' => [
            //         'view' => function ($url) {
            //             return Html::a('', $url, ['class' => 'glyphicon glyphicon-eye-open btn btn-xs btn-default btn-text-info']);
            //         },
            //         'update' => function ($url) {
            //             return Html::a('', $url, ['class' => 'glyphicon glyphicon-pencil btn btn-xs btn-default btn-text-warning']);
            //         },
            //         'delete' => function ($url) {
            //             return Html::a('', $url, [
            //                 'class' => 'glyphicon glyphicon-trash btn btn-xs btn-default btn-text-danger', 
            //                 'data-method' => 'post', 
            //                 'data-confirm' => 'Are you sure you want to delete this item?']);
            //         },
            //     ],
            // ],
            // 'id',
            [
                'attribute' => 'jenis_gangguan_id',
                'value' => 'jenisGangguan.nama_gangguan',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(JenisGangguan::find()->orderBy('nama_gangguan')->asArray()->all(), 'id', 'nama'), 
                'filterInputOptions'=>['placeholder'=>''],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear'=>true],
                ],
            ],
            [
                'attribute' => 'pelanggan_id',
                'value' => 'pelanggan.nama',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Pelanggan::find()->orderBy('nama')->asArray()->all(), 'id', 'nama'), 
                'filterInputOptions'=>['placeholder'=>''],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear'=>true],
                ],
            ],
            [
                'attribute' => 'tanggal',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterInputOptions' => ['placeholder' => ''],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                ],
            ],
            'status',
            [
                'attribute' => 'catatan',
                'format' => 'ntext',
                'contentOptions' => ['class' => 'text-wrap'],
            ],
            // 'created_at:integer',
            // 'updated_at:integer',
            // 'created_by:integer',
            // 'updated_by:integer',
        ];
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'pjax' => true,
        'hover' => true,
        'striped' => false,
        'bordered' => false,
        'toolbar'=> [
            Html::a('<i class="fa fa-plus"></i> ' . 'Create', ['create'], ['class' => 'btn btn-success']),
            Html::a('<i class="fa fa-repeat"></i> ' . 'Reload', ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default']),
            '{toggleData}',
            // $exportMenu,
        ],
        'panel' => [
            'type' => 'no-border transparent',
            'heading' => false,
            'before' => '{summary}',
            'after' => false,
        ],
        'panelBeforeTemplate' => '
            <div class="row">
                <div class="col-sm-8">
                    <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
                        {toolbar}
                    </div> 
                </div>
                <div class="col-sm-4">
                    <div class="pull-right">
                        {before}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        ',
        'pjaxSettings' => ['options' => ['id' => 'grid']],
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]); ?>
<?php Pjax::end(); ?>
</div>