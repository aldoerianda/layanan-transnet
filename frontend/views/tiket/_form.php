<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use backend\models\JenisGangguan;
use backend\models\Pelanggan;

/* @var $this yii\web\View */
/* @var $model backend\models\Tiket */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tiket-form">

<div class="row">
<div class="col-md-8 col-sm-12">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'jenis_gangguan_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(JenisGangguan::find()->all(), 'id', 'nama_gangguan'),
        'options' => ['placeholder' => ''],
        'pluginOptions' => ['allowClear' => true],
    ]); ?>

    <?= $form->field($model, 'pelanggan_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Pelanggan::find()->all(), 'id', 'nama'),
        'options' => ['placeholder' => ''],
        'pluginOptions' => ['allowClear' => true],
    ]); ?>

    <?= $form->field($model, 'tanggal')->widget(DatePicker::classname(), [
        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
        'readonly' => true,
        'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
    ]); ?>

    <!-- <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?> -->

    <?= $form->field($model, 'catatan')->textarea(['rows' => 6]) ?>

    
    <div class="form-panel">
        <div class="row">
    	    <div class="col-sm-12">
    	        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($model->isNewRecord ? 'Create' : 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
	    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>

</div>
