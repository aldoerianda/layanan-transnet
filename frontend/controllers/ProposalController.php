<?php

namespace frontend\controllers;

use Yii;
use backend\models\Proposal;
use backend\models\ProposalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\IntegrityException;

use yii\web\UploadedFile;
use backend\models\ProposalLecturerAttended;

/**
 * ProposalController implements the CRUD actions for Proposal model.
 */
class ProposalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Proposal models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (($model = Proposal::findOne(['user_id' => Yii::$app->user->id])) !== null) {
            $this->redirect(['view', 'id' => $model->id]);
        }

        $searchModel = new ProposalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Proposal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Proposal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRegister()
    {
        $model = new Proposal();
        $model->user_id = Yii::$app->user->id;
        $flag_redirect = 0;
        
        if (Yii::$app->request->post() && $model->save()) {
            $field = 'file_registration_approval';
            $path = Yii::getAlias('@uploads/proposal/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }
            
            $field = 'file_turnitin_proposal';
            $path = Yii::getAlias('@uploads/proposal/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }

            if ($flag_redirect) return $this->redirect(['view', 'id' => $model->id]);
        } 
        return $this->render('register', [
            'model' => $model,
        ]);
    }


    /**
     * Updates an existing Proposal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateRegistration($id)
    {
        $model = $this->findModel($id);
        $flag_redirect = 0;

        if (Yii::$app->request->post() && $model->save()) {
            $field = 'file_registration_approval';
            $path = Yii::getAlias('@uploads/proposal/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }

            $field = 'file_turnitin_proposal';
            $path = Yii::getAlias('@uploads/proposal/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }

            if ($flag_redirect) return $this->redirect(['view', 'id' => $model->id]);
        } 
        return $this->render('update-registration', [
            'model' => $model,
        ]);
    }
    public function actionUpdateScheduling($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->post() && $model->save()) {
            $field = 'file_lecturer_availability';
            $path = Yii::getAlias('@uploads/proposal/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) return $this->redirect(['view', 'id' => $model->id]);
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }
        }
        return $this->render('update-scheduling', [
            'model' => $model,
        ]);
    }
    public function actionUpdatePreSeminar($id)
    {
        $model = $this->findModel($id);
        $flag_redirect = 0;

        if (Yii::$app->request->post() && $model->save()) {
            $field = 'file_invitation';
            $path = Yii::getAlias('@uploads/proposal/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }

            $field = 'file_handover';
            $path = Yii::getAlias('@uploads/proposal/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }
        }
        
        if ($flag_redirect) return $this->redirect(['view', 'id' => $model->id]);
        return $this->render('update-pre-seminar', [
            'model' => $model,
        ]);
    }
    public function actionUpdatePascaSeminar($id)
    {
        $model = $this->findModel($id);
        $flag_redirect = 0;

        $modelItem = new ProposalLecturerAttended();
        $modelItem->proposal_id = $id;
        if (($proposal_lecturer_attended_id = Yii::$app->request->get('proposal_lecturer_attended_id')) !== null) {
            $modelItem = ProposalLecturerAttended::findOne($proposal_lecturer_attended_id);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $field = 'file_minutes_of_meeting';
            $path = Yii::getAlias('@uploads/proposal/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }

            $field = 'file_proposal';
            $path = Yii::getAlias('@uploads/proposal/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }

            $field = 'file_research_approval';
            $path = Yii::getAlias('@uploads/proposal/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }
        } elseif ($modelItem->load(Yii::$app->request->post())) {
            if ($modelItem->save()) {
                return $this->redirect(['update-pasca-seminar', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($modelItem->errors));
            }
        } elseif(Yii::$app->request->post()) {dd(Yii::$app->request->post()); }

        if ($flag_redirect) return $this->redirect(['view', 'id' => $model->id]);
        return $this->render('update-pasca-seminar', [
            'model' => $model,
            'modelItem' => $modelItem,
        ]);
    }

    /**
     * Deletes an existing Proposal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } catch (IntegrityException $e) {
            throw new \yii\web\HttpException(500,"Integrity Constraint Violation. This data can not be deleted due to the relation.", 405);
        }
    }

    /**
     * Finds the Proposal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Proposal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Proposal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionDownload($id, $field)
    {
        $model = $this->findModel($id);
        
        if ($model->$field) {
            $filepath = Yii::getAlias('@uploads/proposal/' . $field . '/' . $model->$field);
            $filename = str_replace($model->id, $model->getAttributeLabel($field), $model->$field);

            if (file_exists($filepath)) Yii::$app->response->sendFile($filepath, $filename, ['inline' => true]);
        }
    }
}
