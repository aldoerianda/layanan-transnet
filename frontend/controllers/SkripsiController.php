<?php

namespace frontend\controllers;

use Yii;
use backend\models\Skripsi;
use backend\models\SkripsiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\db\IntegrityException;
use backend\models\SkripsiLecturerAttended;

/**
 * SkripsiController implements the CRUD actions for Skripsi model.
 */
class SkripsiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Skripsi models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (($model = Skripsi::findOne(['user_id' => Yii::$app->user->id])) !== null) {
            $this->redirect(['view', 'id' => $model->id]);
        }

        $searchModel = new SkripsiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Skripsi model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Skripsi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRegister()
    {
        $model = new Skripsi();
        $model->user_id = Yii::$app->user->id;
        $flag_redirect = 0;

        if (Yii::$app->request->post()) {

            $model->load(Yii::$app->request->post());

            if ($model->save()) $flag_redirect = 1;
            else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));

            $field = 'file_approval';
            $path = Yii::getAlias('@uploads/skripsi/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }

            $field = 'file_toefl';
            $path = Yii::getAlias('@uploads/skripsi/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }

            $field = 'file_turnitin_article';
            $path = Yii::getAlias('@uploads/skripsi/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }

            $field = 'file_turnitin_skripsi';
            $path = Yii::getAlias('@uploads/skripsi/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }

            $field = 'file_publishing';
            $path = Yii::getAlias('@uploads/skripsi/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }
        }

        if ($flag_redirect) return $this->redirect(['view', 'id' => $model->id]);
        return $this->render('register', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Skripsi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateRegistration($id)
    {
        $model = $this->findModel($id);

        $flag_redirect = 0;

        if (Yii::$app->request->post() && $model->save()) {
            $model->load(Yii::$app->request->post());
            $model->save();

            $field = 'file_approval';
            $path = Yii::getAlias('@uploads/skripsi/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }

            $field = 'file_toefl';
            $path = Yii::getAlias('@uploads/skripsi/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }

            $field = 'file_turnitin_article';
            $path = Yii::getAlias('@uploads/skripsi/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }

            $field = 'file_turnitin_skripsi';
            $path = Yii::getAlias('@uploads/skripsi/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }

            $field = 'file_publishing';
            $path = Yii::getAlias('@uploads/skripsi/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }
        }

        if ($flag_redirect) return $this->redirect(['view', 'id' => $model->id]);
        return $this->render('update-registration', [
            'model' => $model,
        ]);
    }
    public function actionUpdateScheduling($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->post() && $model->save()) {
            $field = 'file_lecturer_availability';
            $path = Yii::getAlias('@uploads/skripsi/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) return $this->redirect(['view', 'id' => $model->id]);
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }
        }
        return $this->render('update-scheduling', [
            'model' => $model,
        ]);
    }
    public function actionUpdatePreExamination($id)
    {
        $model = $this->findModel($id);
        $flag_redirect = 0;

        if (Yii::$app->request->post() && $model->save()) {
            $field = 'file_invitation';
            $path = Yii::getAlias('@uploads/skripsi/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }

            $field = 'file_handover';
            $path = Yii::getAlias('@uploads/skripsi/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }
        }
        
        if ($flag_redirect) return $this->redirect(['view', 'id' => $model->id]);
        return $this->render('update-pre-examination', [
            'model' => $model,
        ]);
    }
    public function actionUpdatePascaExamination($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update-by-student';
        $flag_redirect = 0;

        $modelItem = new SkripsiLecturerAttended();
        $modelItem->skripsi_id = $id;
        if (($skripsi_lecturer_attended_id = Yii::$app->request->get('skripsi_lecturer_attended_id')) !== null) {
            $modelItem = SkripsiLecturerAttended::findOne($skripsi_lecturer_attended_id);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $flag_redirect = 1;

            $field = 'file_minutes_of_meeting';
            $path = Yii::getAlias('@uploads/skripsi/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }

            $field = 'file_article';
            $path = Yii::getAlias('@uploads/skripsi/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }

            $field = 'file_skripsi';
            $path = Yii::getAlias('@uploads/skripsi/' . $field . '/');
            $file = UploadedFile::getInstanceByName($field);
            if ($file) {
                $filename = $model->id . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $model->$field = $filename;
                    if ($model->save()) $flag_redirect = 1;
                    else Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($model->errors));
                }
            }
        } elseif ($modelItem->load(Yii::$app->request->post())) {
            if ($modelItem->save()) {
                return $this->redirect(['update-pasca-examination', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('error', \yii\helpers\Json::encode($modelItem->errors));
            }
        }

        if ($flag_redirect) return $this->redirect(['view', 'id' => $model->id]);
        return $this->render('update-pasca-examination', [
            'model' => $model,
            'modelItem' => $modelItem,
        ]);
    }

    /**
     * Deletes an existing Skripsi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } catch (IntegrityException $e) {
            throw new \yii\web\HttpException(500,"Integrity Constraint Violation. This data can not be deleted due to the relation.", 405);
        }
    }

    /**
     * Finds the Skripsi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Skripsi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Skripsi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionDownload($id, $field)
    {
        $model = $this->findModel($id);
        
        if ($model->$field) {
            $filepath = Yii::getAlias('@uploads/skripsi/' . $field . '/' . $model->$field);
            $filename = str_replace($model->id, $model->getAttributeLabel($field), $model->$field);

            if (file_exists($filepath)) Yii::$app->response->sendFile($filepath, $filename, ['inline' => true]);
        }
    }
}
