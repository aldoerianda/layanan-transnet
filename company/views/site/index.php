<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
Yii::$app->params['showTitle'] = false;
?>

<div class="jumbotron" style="opacity:0.4">
    <h1><b><?= Yii::$app->user->identity->name ?></b></h1>
    <!-- <p class="lead"><?= Yii::$app->name ?></p> -->
</div>

<div class="row">
<div class="col-sm-4">
    <div class="small-box bg-aqua">
        <div class="inner">
            <h3><?= \backend\models\Employee::find()->where(['company_id' => Yii::$app->user->id])->count() ?></h3>
            <p>Pegawai</p>
        </div>
        <div class="icon">
            <i class="fa fa-street-view"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>
<div class="col-sm-4">
    <div class="small-box bg-aqua">
        <div class="inner">
            <h3><?= Yii::$app->formatter->asDecimal(\backend\models\Payment::find()->joinWith(['employee'])->where(['company_id' => Yii::$app->user->id])->sum('salary'), 0) ?></h3>
            <p>Total Pembayaran Gaji</p>
        </div>
        <div class="icon">
            <i class="fa fa-money"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>
<div class="col-sm-4">
    <div class="small-box bg-aqua">
        <div class="inner">
            <h3><?= Yii::$app->formatter->asDecimal(\backend\models\Payment::find()->joinWith(['employee'])->where(['company_id' => Yii::$app->user->id])->sum('allowance'), 0) ?></h3>
            <p>Total Pembayaran Tunjangan</p>
        </div>
        <div class="icon">
            <i class="fa fa-envelope"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>
</div>

<div class="detail-view-container">
<?= DetailView::widget([
    'options' => ['class' => 'table detail-view'],
    'model' => $model,
    'attributes' => [
        // 'id',
        'name',
        'username',
        // 'password_hash',
        'address',
        'phone',
        // 'created_at:datetime',
        // 'updated_at:datetime',
        // 'createdBy.username:text:Created By',
        // 'updatedBy.username:text:Updated By',
    ],
]) ?>
</div>