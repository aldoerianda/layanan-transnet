function retrieveSalary(employee_id) {
	$('#payment-salary').val('');
	$('#payment-allowance').val('');
	$.post("?r=payment/get-salary", { employee_id: employee_id }, function (response) {
		$('#payment-salary').val(response.grade.salary);
		$('#payment-allowance').val(response.grade.allowance);
	}, "json");
}

$(document).ready(function() {
	$('#payment-employee_id').on("change", function (e) {
		retrieveSalary(this.value);
	});
});
